#!/bin/sh

## Copy sources file (no longer needed)
# sudo cp etc/apt/sources.list /etc/apt/sources.list

## Append stable repositories to the end of sources.list
sudo echo ' ' >> /etc/apt/sources.list
sudo echo '# Stable repositories (needed to install Java)' /etc/apt/sources.list
sudo echo ' ' >> /etc/apt/sources.list
sudo echo 'deb http://deb.debian.org/debian/ stable main non-free contrib' >> /etc/apt/sources.list
sudo echo 'deb-src http://deb.debian.org/debian/ stable main non-free contrib' >> /etc/apt/sources.list

## Add multi-architecture support
sudo dpkg --add-architecture i386

## Update repositories
sudo apt update -y

## Install default software
sudo apt-get install -f -y i3 xorg pulseaudio pavucontrol xbindkeys ranger firefox-esr lxappearance network-manager network-manager-gnome fonts-noto neofetch scrot rofi feh mupdf thunar file-roller gtk2-engines gtk2-engines-murrine mpv gedit gdebi i3blocks i3bar adapta-gtk-theme breeze-cursor-theme fortune cowsay papirus-adapta-icons rxvt-unicode volumeicon-alsa compton clipit gvfs-backends gvfs-fuse wget aptitude

## Create default directories
mkdir ~/Pictures/
mkdir ~/Downloads/
mkdir ~/Documents/
mkdir ~/Screenshot/
mkdir ~/.config/
mkdir ~/.config/i3/
mkdir ~/.config/i3blocks/
mkdir ~/.config/gtk-3.0/
mkdir ~/.config/rofi/

## Copy config files
cp .Xdefaults ~/
cp .bash_aliases ~/
cp .fehbg ~/
cp .gtkrc-2.0 ~/
cp .xbindkeysrc ~/
cp .xsessionrc ~/
cp .config/i3/config ~/.config/i3/config
cp .config/i3blocks/config ~/.config/i3blocks/config
cp .config/mimeapps.list ~/.config/
cp .config/rofi/config ~/.config/rofi/
cp .config/gtk-3.0/settings.ini ~/.config/gtk-3.0/
cp .config/compton.conf ~/.config/
cp .config/clipit/clipitrc ~/.config/clipit/
sudo cp etc/sudoers.d/SD /etc/sudoers.d/SD
sudo cp etc/systemd/logind.conf /etc/systemd/logind.conf 
sudo cp usr/share/i3blocks/battery_icon /usr/share/i3blocks/ 
sudo cp usr/share/i3blocks/bandwidth_icon /usr/share/i3blocks/ 

## Copy ranger executable (for use with rofi)
chmod +x usr/local/bin/stranger
sudo cp usr/local/bin/stranger /usr/local/bin/

## Disable gvfs auto-mounting network locations (prevents Thunar from hanging on first startup)
sudo sed -i s/AutoMount=true/AutoMount=false/ /usr/share/gvfs/mounts/network.mount

## Install i3-gaps
chmod +x i3-gaps-deb.sh
./i3-gaps-deb.sh

########

chmod +x postinstall.sh
chmod +x mc.sh
echo 'Initial setup complete, please reboot and run postinstall.sh to install LaTeX, Pandoc,'
echo 'youtube-dl and other utilities, and mc.sh to install JRE 8 and Minecraft (optional).'