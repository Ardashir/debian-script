## TODO

* Upload ranger's config files to this repository
* Config URxvt and upload the files to this repository
* Config mpv and upload the files to this repository


## Adding a user to the 'sudo' group

If a root password was set during the Debian installation process, you must first add your user to the sudoers group.

1. Log in as root.
2. Install **sudo**: *apt install sudo*
3. Add your user to the **sudo** group: *usermod -a -G sudo [USERNAME]*
4. Log into your user account and execute the **run.sh** script normally.