#/bin/sh

## Add stable repositores to sources.list
## (commented out because this is already done in run.sh)
# sudo echo ' ' >> /etc/apt/sources.list
# sudo echo '# Stable repositories (needed to install Java)' /etc/apt/sources.list
# sudo echo ' ' >> /etc/apt/sources.list
# sudo echo 'deb http://deb.debian.org/debian/ stable main non-free contrib' >> /etc/apt/sources.list
# sudo echo 'deb-src http://deb.debian.org/debian/ stable main non-free contrib' >> /etc/apt/sources.list

## Download Minecraft launcher from Mojang's website
wget https://launcher.mojang.com/download/Minecraft.deb

## Install Java
sudo apt install -f -y openjdk-8-jre 

## Install Minecraft launcher and remove the installer file
sudo dpkg -i Minecraft.deb 
rm Minecraft.deb